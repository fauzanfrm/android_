package com.example.mahardika.apscountnumber;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class OrderActivity extends AppCompatActivity {

    int Counter, Counter1, Counter2, Counter3, Counter4, Counter5, Counter6;
    Button tambah, tambah1, tambah2, tambah3, tambah4, tambah5, tambah6, kurang, kurang1, kurang2, kurang3, kurang4, kurang5, kurang6, reset, reset1, reset2, reset3, reset4, reset5, reset6;
//    EditText nilai, nilai1, nilai2, nilai3, nilai4, nilai5, nilai6;
    EditText etcountnasi, etcountuduk, etcountayam, etcounttempe, etcounttahu, etcountestehmanis, etcountair;
    Context context;
    String header;
    TextView tvharganasi, tvhargauduk, tvhargaayam, tvhargatempe, tvhargatahu, tvhargaesteh, tvhargaairputih, tvtotal,tvtotalpesan;
    int hrgnasi = 4000;
    int hrguduk = 6000;
    int hrgayam = 15000;
    int hrgtempe = 8000;
    int hrgtahu = 8000;
    int hrgesteh = 3000;
    int hrgairputih = 1000;
    int hrgtotal = 0;
    int totalpesan = 0;
    int akumulasi,akumulasi1,akumulasi2,akumulasi3,akumulasi4,akumulasi5,akumulasi6;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        //Fungsi Button Tambah, Kurang dan Reset di Halaman Pemesanan Menu makanan/minuman
        //utk harga nasi

        final EditText etdata1 = (EditText) findViewById(R.id.etcountnasi);
        final EditText etdata2 = (EditText) findViewById(R.id.etcountuduk);
        final EditText etdata3 = (EditText) findViewById(R.id.etcountayam);
        final EditText etdata4 = (EditText) findViewById(R.id.etcounttempe);
        final EditText etdata5 = (EditText) findViewById(R.id.etcounttahu);
        final EditText etdata6 = (EditText) findViewById(R.id.etcountestehmanis);
        final EditText etdata7 = (EditText) findViewById(R.id.etcountair);
        final TextView etdata8 = (TextView) findViewById(R.id.tvtotal);


        tvtotal = (TextView) findViewById(R.id.tvtotal);
        tvtotalpesan = (TextView) findViewById(R.id.tvtotalpesan);

        tvharganasi = (TextView) findViewById(R.id.tvharganasi);
        etcountnasi = (EditText) findViewById(R.id.etcountnasi);
        kurang = (Button) findViewById(R.id.btnKurang);
        kurang.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist = Integer.valueOf(tvharganasi.getText().toString());
                akumulasi = hrgExist - hrgnasi;
                Counter--;
                if (akumulasi < 0) {
                    tvharganasi.setText("0");
                    etcountnasi.setText("0");
                } else {
                    tvharganasi.setText(String.valueOf(akumulasi));
                    etcountnasi.setText(""+Counter);
                    hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                    tvtotal.setText(String.valueOf(hrgtotal));
                    totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                    tvtotalpesan.setText(String.valueOf(totalpesan));
                }
            }
        });

        tambah = (Button) findViewById(R.id.btnTambah);
        tambah.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist = Integer.valueOf(tvharganasi.getText().toString());
                akumulasi = hrgExist + hrgnasi;
                tvharganasi.setText(String.valueOf(akumulasi));
                Counter++;
                etcountnasi.setText(""+Counter);
                hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                tvtotal.setText(String.valueOf(hrgtotal));
                totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                tvtotalpesan.setText(String.valueOf(totalpesan));
            }
        });

        //////////// utk Harga Uduk
        tvhargauduk = (TextView) findViewById(R.id.tvhargauduk);
        etcountuduk = (EditText) findViewById(R.id.etcountuduk);
        kurang1 = (Button) findViewById(R.id.btnKurang1);
        kurang1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist1 = Integer.valueOf(tvhargauduk.getText().toString());
                akumulasi1 = hrgExist1 - hrguduk;
                Counter1--;
                if (akumulasi1 < 0) {
                    tvhargauduk.setText("0");
                    etcountuduk.setText("0");
                } else {
                    tvhargauduk.setText(String.valueOf(akumulasi1));
                    etcountuduk.setText(""+Counter1);
                    hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                    tvtotal.setText(String.valueOf(hrgtotal));
                    totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                    tvtotalpesan.setText(String.valueOf(totalpesan));
                }
            }
        });

        tambah1 = (Button) findViewById(R.id.btnTambah1);
        tambah1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist1 = Integer.valueOf(tvhargauduk.getText().toString());
                akumulasi1 = hrgExist1 + hrguduk;
                tvhargauduk.setText(String.valueOf(akumulasi1));
                Counter1++;
                etcountuduk.setText(""+Counter1);
                hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                tvtotal.setText(String.valueOf(hrgtotal));
                totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                tvtotalpesan.setText(String.valueOf(totalpesan));
            }
        });

        //////////// utk Harga Ayam Goreng
        tvhargaayam = (TextView) findViewById(R.id.tvhargaayam);
        etcountayam = (EditText) findViewById(R.id.etcountayam);
        kurang2 = (Button) findViewById(R.id.btnKurang2);
        kurang2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist2 = Integer.valueOf(tvhargaayam.getText().toString());
                akumulasi2 = hrgExist2 - hrgayam;
                Counter2--;
                if (akumulasi2 < 0) {
                    tvhargaayam.setText("0");
                    etcountayam.setText("0");
                } else {
                    tvhargaayam.setText(String.valueOf(akumulasi2));
                    etcountayam.setText(""+Counter2);
                    hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                    tvtotal.setText(String.valueOf(hrgtotal));
                    totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                    tvtotalpesan.setText(String.valueOf(totalpesan));
                }
            }
        });

        tambah2 = (Button) findViewById(R.id.btnTambah2);
        tambah2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist2 = Integer.valueOf(tvhargaayam.getText().toString());
                akumulasi2 = hrgExist2 + hrgayam;
                tvhargaayam.setText(String.valueOf(akumulasi2));
                Counter2++;
                etcountayam.setText(""+Counter2);
                hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                tvtotal.setText(String.valueOf(hrgtotal));
                totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                tvtotalpesan.setText(String.valueOf(totalpesan));
            }
        });

        //////////// utk Harga Tempe Goreng
        tvhargatempe = (TextView) findViewById(R.id.tvhargatempe);
        etcounttempe = (EditText) findViewById(R.id.etcounttempe);
        kurang3 = (Button) findViewById(R.id.btnKurang3);
        kurang3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist3 = Integer.valueOf(tvhargatempe.getText().toString());
                akumulasi3 = hrgExist3 - hrgtempe;
                Counter3--;
                if (akumulasi3 < 0) {
                    tvhargatempe.setText("0");
                    etcounttempe.setText("0");
                } else {
                    tvhargatempe.setText(String.valueOf(akumulasi3));
                    etcounttempe.setText(""+Counter3);
                    hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                    tvtotal.setText(String.valueOf(hrgtotal));
                    totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                    tvtotalpesan.setText(String.valueOf(totalpesan));
                }
            }
        });

        tambah3 = (Button) findViewById(R.id.btnTambah3);
        tambah3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist3 = Integer.valueOf(tvhargatempe.getText().toString());
                akumulasi3 = hrgExist3 + hrgtempe;
                tvhargatempe.setText(String.valueOf(akumulasi3));
                Counter3++;
                etcounttempe.setText(""+Counter3);
                hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                tvtotal.setText(String.valueOf(hrgtotal));
                totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                tvtotalpesan.setText(String.valueOf(totalpesan));
            }
        });

        //////////// utk Harga Tahu Goreng
        tvhargatahu = (TextView) findViewById(R.id.tvhargatahu);
        etcounttahu = (EditText) findViewById(R.id.etcounttahu);
        kurang4 = (Button) findViewById(R.id.btnKurang4);
        kurang4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist4 = Integer.valueOf(tvhargatahu.getText().toString());
                akumulasi4 = hrgExist4 - hrgtahu;
                Counter4--;
                if (akumulasi4 < 0) {
                    tvhargatahu.setText("0");
                    etcounttahu.setText("0");
                } else {
                    tvhargatahu.setText(String.valueOf(akumulasi4));
                    etcounttahu.setText(""+Counter4);
                    hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                    tvtotal.setText(String.valueOf(hrgtotal));
                    totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                    tvtotalpesan.setText(String.valueOf(totalpesan));
                }
            }
        });

        tambah4 = (Button) findViewById(R.id.btnTambah4);
        tambah4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist4 = Integer.valueOf(tvhargatahu.getText().toString());
                akumulasi4 = hrgExist4 + hrgtahu;
                tvhargatahu.setText(String.valueOf(akumulasi4));
                Counter4++;
                etcounttahu.setText(""+Counter4);
                hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                tvtotal.setText(String.valueOf(hrgtotal));
                totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                tvtotalpesan.setText(String.valueOf(totalpesan));
            }
        });

        //////////// utk Harga Es Teh Manis
        tvhargaesteh = (TextView) findViewById(R.id.tvhargaesteh);
        etcountestehmanis = (EditText) findViewById(R.id.etcountestehmanis);
        kurang5 = (Button) findViewById(R.id.btnKurang5);
        kurang5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist5 = Integer.valueOf(tvhargaesteh.getText().toString());
                akumulasi5 = hrgExist5 - hrgesteh;
                Counter5--;
                if (akumulasi5 < 0) {
                    tvhargaesteh.setText("0");
                    etcountestehmanis.setText("0");
                } else {
                    tvhargaesteh.setText(String.valueOf(akumulasi5));
                    etcountestehmanis.setText(""+Counter5);
                    hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                    tvtotal.setText(String.valueOf(hrgtotal));
                    totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                    tvtotalpesan.setText(String.valueOf(totalpesan));
                }
            }
        });

        tambah5 = (Button) findViewById(R.id.btnTambah5);
        tambah5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist5 = Integer.valueOf(tvhargaesteh.getText().toString());
                akumulasi5 = hrgExist5 + hrgesteh;
                tvhargaesteh.setText(String.valueOf(akumulasi5));
                Counter5++;
                etcountestehmanis.setText(""+Counter5);
                hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                tvtotal.setText(String.valueOf(hrgtotal));
                totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                tvtotalpesan.setText(String.valueOf(totalpesan));
            }
        });

        //////////// utk Harga Air Putih
        tvhargaairputih = (TextView) findViewById(R.id.tvhargaairputih);
        etcountair = (EditText) findViewById(R.id.etcountair);
        kurang6 = (Button) findViewById(R.id.btnKurang6);
        kurang6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist6 = Integer.valueOf(tvhargaairputih.getText().toString());
                akumulasi6 = hrgExist6 - hrgairputih;
                Counter6--;
                if (akumulasi6 < 0) {
                    tvhargaairputih.setText("0");
                    etcountair.setText("0");
                } else {
                    tvhargaairputih.setText(String.valueOf(akumulasi6));
                    etcountair.setText(""+Counter6);
                    hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                    tvtotal.setText(String.valueOf(hrgtotal));
                    totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                    tvtotalpesan.setText(String.valueOf(totalpesan));
                }
            }
        });

        tambah6 = (Button) findViewById(R.id.btnTambah6);
        tambah6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hrgExist6 = Integer.valueOf(tvhargaairputih.getText().toString());
                akumulasi6 = hrgExist6 + hrgairputih;
                tvhargaairputih.setText(String.valueOf(akumulasi6));
                Counter6++;
                etcountair.setText(""+Counter6);
                hrgtotal = akumulasi+akumulasi1+akumulasi2+akumulasi3+akumulasi4+akumulasi5+akumulasi6;
                tvtotal.setText(String.valueOf(hrgtotal));
                totalpesan = Counter+Counter1+Counter2+Counter3+Counter4+Counter5+Counter6;
                tvtotalpesan.setText(String.valueOf(totalpesan));
            }
        });

        Button btnpesan = (Button)findViewById(R.id.btnpesan);
        btnpesan.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                Intent pindah = new Intent(OrderActivity.this, HasilOrderActivity.class);
                pindah.putExtra("data1", etdata1.getText().toString());
                pindah.putExtra("data2", etdata2.getText().toString());
                pindah.putExtra("data3", etdata3.getText().toString());
                pindah.putExtra("data4", etdata4.getText().toString());
                pindah.putExtra("data5", etdata5.getText().toString());
                pindah.putExtra("data6", etdata6.getText().toString());
                pindah.putExtra("data7", etdata7.getText().toString());
                pindah.putExtra("data8", etdata8.getText().toString());
                startActivity(pindah);
            }
        });





    }

}



//    double akumulasi = Double.parseDouble(String.valueOf(tvharganasi.getText()));
//    double akumulasi1 = Double.parseDouble(String.valueOf(tvhargauduk.getText()));
//    double total = akumulasi+akumulasi1;
//    tvtotal.setText(String.valueOf(total));



//        context = this;
//        tambah = (Button) findViewById(R.id.btnTambah);
//        tambah1 = (Button) findViewById(R.id.btnTambah1);
//        tambah2 = (Button) findViewById(R.id.btnTambah2);
//        tambah3 = (Button) findViewById(R.id.btnTambah3);
//        tambah4 = (Button) findViewById(R.id.btnTambah4);
//        tambah5 = (Button) findViewById(R.id.btnTambah5);
//        tambah6 = (Button) findViewById(R.id.btnTambah6);
//
//        kurang = (Button) findViewById(R.id.btnKurang);
//        kurang1 = (Button) findViewById(R.id.btnKurang1);
//        kurang2 = (Button) findViewById(R.id.btnKurang2);
//        kurang3 = (Button) findViewById(R.id.btnKurang3);
//        kurang4 = (Button) findViewById(R.id.btnKurang4);
//        kurang5 = (Button) findViewById(R.id.btnKurang5);
//        kurang6 = (Button) findViewById(R.id.btnKurang6);
//
//        reset = (Button) findViewById(R.id.btnReset);
//        reset1 = (Button) findViewById(R.id.btnReset1);
//        reset2 = (Button) findViewById(R.id.btnReset2);
//        reset3 = (Button) findViewById(R.id.btnReset3);
//        reset4 = (Button) findViewById(R.id.btnReset4);
//        reset5 = (Button) findViewById(R.id.btnReset5);
//        reset6 = (Button) findViewById(R.id.btnReset6);
//
//        nilai = (EditText) findViewById(R.id.etcountnasi);
//        nilai1 = (EditText) findViewById(R.id.etcountuduk);
//        nilai2 = (EditText) findViewById(R.id.etcountayam);
//        nilai3 = (EditText) findViewById(R.id.etcounttempe);
//        nilai4 = (EditText) findViewById(R.id.etcounttahu);
//        nilai5 = (EditText) findViewById(R.id.etcountestehmanis);
//        nilai6 = (EditText) findViewById(R.id.etcountair);
//
//        nilai.setText("" + Counter);
//        nilai1.setText("" + Counter);
//        nilai2.setText("" + Counter);
//        nilai3.setText("" + Counter);
//        nilai4.setText("" + Counter);
//        nilai5.setText("" + Counter);
//        nilai6.setText("" + Counter);


//        tambah.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter++;
//                nilai.setText("" + Counter);
//            }
//        });
//
//        kurang.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter--;
//                nilai.setText("" + Counter);
//
//            }
//        });
//
//        reset.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter = 0;
//                nilai.setText("" + Counter);
//            }
//        });
//
//        //////////////////////////////////////
//
//        tambah1.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter1++;
//                nilai1.setText("" + Counter1);
//            }
//        });
//
//        kurang1.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter1--;
//                nilai1.setText("" + Counter1);
//
//            }
//        });
//
//        reset1.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter1 = 0;
//                nilai1.setText("" + Counter1);
//            }
//        });
//
//        //////////////////////////////////////
//
//        tambah2.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter2++;
//                nilai2.setText("" + Counter2);
//            }
//        });
//
//        kurang2.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter2--;
//                nilai2.setText("" + Counter2);
//
//            }
//        });
//
//        reset2.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter2 = 0;
//                nilai2.setText("" + Counter2);
//            }
//        });
//
//        //////////////////////////////////////
//
//        tambah3.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter3++;
//                nilai3.setText("" + Counter3);
//            }
//        });
//
//        kurang3.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter3--;
//                nilai3.setText("" + Counter3);
//
//            }
//        });
//
//        reset3.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter3 = 0;
//                nilai3.setText("" + Counter3);
//            }
//        });
//
//        //////////////////////////////////////
//
//        tambah4.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter4++;
//                nilai4.setText("" + Counter4);
//            }
//        });
//
//        kurang4.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter4--;
//                nilai4.setText("" + Counter4);
//
//            }
//        });
//
//        reset4.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter4 = 0;
//                nilai4.setText("" + Counter4);
//            }
//        });
//
//        //////////////////////////////////////
//
//        tambah5.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter5++;
//                nilai5.setText("" + Counter5);
//            }
//        });
//
//        kurang5.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter5--;
//                nilai5.setText("" + Counter5);
//
//            }
//        });
//
//        reset5.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter5 = 0;
//                nilai5.setText("" + Counter5);
//            }
//        });
//
//        //////////////////////////////////////
//
//        tambah6.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter6++;
//                nilai6.setText("" + Counter6);
//            }
//        });
//
//        kurang6.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter6--;
//                nilai6.setText("" + Counter6);
//
//            }
//        });
//
//        reset6.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Counter6 = 0;
//                nilai6.setText("" + Counter6);
//            }
//        });

