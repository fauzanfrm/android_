package com.example.mahardika.apscountnumber;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

public class JenisPesan extends AppCompatActivity {

    RadioButton rbketmakan, rbketbungkus;
    Button next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jenis_pesan);

        rbketmakan = (RadioButton) findViewById(R.id.rbketmakan);
        rbketbungkus = (RadioButton) findViewById(R.id.rbketbungkus);
        next = (Button) findViewById(R.id.btnNext);

        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (rbketbungkus.isChecked()){
                    Intent intent = new Intent(JenisPesan.this, OrderActivity.class);
                    startActivity(intent);
                }
                else if (rbketmakan.isChecked()){
                    Intent intent = new Intent(JenisPesan.this, MainActivity.class);
                    startActivity(intent);
                }
            }
            });

    }
}

