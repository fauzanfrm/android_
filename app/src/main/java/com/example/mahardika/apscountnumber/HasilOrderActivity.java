package com.example.mahardika.apscountnumber;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class HasilOrderActivity extends AppCompatActivity {

    BluetoothAdapter bluetoothAdapter;
    BluetoothSocket bluetoothSocket;
    BluetoothDevice bluetoothDevice;
    private Set<BluetoothDevice> pairedDevices;

    private static OutputStream outputStream;
    InputStream inputStream;
    Thread thread;

    byte FONT_TYPE;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;


    Button total, kembalian, struk, btnTotalBayar;
    Integer kembalianda;
    EditText etUang;
    TextView lblPrinterName, tvJudulBiaya, tvTotalBayar, tvUangKembalian;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_order);


        final Integer biaya;
        final Integer pajak;
        final Integer tbayar;

        TextView tvdata1 = (TextView) findViewById(R.id.tvPesan1);
        TextView tvdata2 = (TextView) findViewById(R.id.tvPesan2);
        TextView tvdata3 = (TextView) findViewById(R.id.tvPesan3);
        TextView tvdata4 = (TextView) findViewById(R.id.tvPesan4);
        TextView tvdata5 = (TextView) findViewById(R.id.tvPesan5);
        TextView tvdata6 = (TextView) findViewById(R.id.tvPesan6);
        TextView tvdata7 = (TextView) findViewById(R.id.tvPesan7);
        TextView tvdata8 = (TextView) findViewById(R.id.tvJudulBiaya);
        TextView tvNilaiPajak = (TextView) findViewById(R.id.tvNilaiPajak);
        tvTotalBayar = (TextView) findViewById(R.id.tvTotalBayar);
        tvJudulBiaya = (TextView) findViewById(R.id.tvJudulBiaya);
        tvUangKembalian = (TextView) findViewById(R.id.tvUangKembalian);
        lblPrinterName = (TextView) findViewById(R.id.lblPrinterName);

        etUang = (EditText) findViewById(R.id.etUang);
        struk = (Button) findViewById(R.id.btnCetakStruk);
        kembalian = (Button) findViewById(R.id.btnKembali);
        btnTotalBayar = (Button) findViewById(R.id.btnTotalBayar);

//        Button btnCetakStruk = (Button) findViewById(R.id.btnCetakStruk);
//        Button btnDapur = (Button) findViewById(R.id.btnDapur);

//        tvdata8 = (TextView) findViewById(R.id.tvJudulBiaya);
//        tvNilaiPajak = (TextView) findViewById(R.id.tvNilaiPajak);


        try{
            FindBluetoothDevice();
            openBluetoothPrinter();

        }catch (Exception ex){
            ex.printStackTrace();
        }
        if (getIntent().getExtras()!=null){
            tvdata1.setText(getIntent().getStringExtra("data1"));
            tvdata2.setText(getIntent().getStringExtra("data2"));
            tvdata3.setText(getIntent().getStringExtra("data3"));
            tvdata4.setText(getIntent().getStringExtra("data4"));
            tvdata5.setText(getIntent().getStringExtra("data5"));
            tvdata6.setText(getIntent().getStringExtra("data6"));
            tvdata7.setText(getIntent().getStringExtra("data7"));
            tvdata8.setText(getIntent().getStringExtra("data8"));
        }

            total = (Button) findViewById(R.id.btnTotalBayar);
            biaya = Integer.parseInt(tvdata8.getText().toString());
            pajak = Integer.parseInt(tvNilaiPajak.getText().toString());
//            uanganda = Integer.parseInt(etUang.getText().toString());

            Integer hasil=biaya*pajak;
            Integer kurangpajak=hasil/100;
            final int totalbayar=biaya+kurangpajak;
            tvTotalBayar.setText(String.valueOf(totalbayar));

            total.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){

                }
            });

            struk.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    try{
                        printData();
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            });

            kembalian.setOnClickListener(new View.OnClickListener(){
                public void onClick (View v){
                    Integer uang1 = Integer.parseInt(etUang.getText().toString());
                    Integer uang2 = Integer.parseInt(tvTotalBayar.getText().toString());
                    kembalianda = uang1 - uang2;
                    tvUangKembalian.setText(String.valueOf(kembalianda));

                }
            });
    }

    public void FindBluetoothDevice(){

        try{

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(bluetoothAdapter==null){
                lblPrinterName.setText("No Bluetooth Adapter found");
            }
            if(bluetoothAdapter.isEnabled()){
                Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBT,1);
            }

            pairedDevices = bluetoothAdapter.getBondedDevices();

            if(pairedDevices.size()>0)
            {
                for(BluetoothDevice pairedDev : pairedDevices)
                {
                    if(pairedDev.getName().equals("BlueTooth Printer")){
                        bluetoothDevice=pairedDev;
                        lblPrinterName.setText("Bluetooth Printer Attached: "+ pairedDev.getName());
                        break;
                    }
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    // Open Bluetooth Printer

    public void openBluetoothPrinter() throws IOException {
        try{

            //Standard uuid from string //
            UUID uuidSting = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            bluetoothSocket=bluetoothDevice.createRfcommSocketToServiceRecord(uuidSting);
            bluetoothSocket.connect();
            outputStream=bluetoothSocket.getOutputStream();
            inputStream=bluetoothSocket.getInputStream();

            beginListenData();

        }catch (Exception ex){

        }
    }

    public void beginListenData(){
        try{

            final Handler handler =new Handler();
            final byte delimiter=10;
            stopWorker =false;
            readBufferPosition=0;
            readBuffer = new byte[1024];

            thread=new Thread(new Runnable() {
                @Override
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker){
                        try{
                            int byteAvailable = inputStream.available();
                            if(byteAvailable>0){
                                byte[] packetByte = new byte[byteAvailable];
                                inputStream.read(packetByte);

                                for(int i=0; i<byteAvailable; i++){
                                    byte b = packetByte[i];
                                    if(b==delimiter){
                                        byte[] encodedByte = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer,0,
                                                encodedByte,0,
                                                encodedByte.length
                                        );
                                        final String data = new String(encodedByte,"US-ASCII");
                                        readBufferPosition=0;
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                lblPrinterName.setText(data);
                                            }
                                        });
                                    }else{
                                        readBuffer[readBufferPosition++]=b;
                                    }
                                }
                            }
                        }catch(Exception ex){
                            stopWorker=true;
                        }
                    }

                }
            });

            thread.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void printData() throws  IOException{
        try{
            printText("     >>>>   Thank you  <<<<     ");
            printCustom("     Rumah Makan Sederhana     ",1,1);
            printCustom("Total Belanja  : " + tvTotalBayar.getText().toString(),1,0);
            printCustom("Tunai          : " + etUang.getText().toString(),1,0);
            printCustom("Kembali        : " + tvUangKembalian.getText().toString(),1,0);
            lblPrinterName.setText("Printing Text...");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void printText(String msg) {
        try {
            // Print normal text
            outputStream.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void printCustom(String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B,0x21,0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B,0x21,0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B,0x21,0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B,0x21,0x10}; // 3- bold with large text
        try {
            switch (size){
                case 0:
                    outputStream.write(cc);
                    break;
                case 1:
                    outputStream.write(bb);
                    break;
                case 2:
                    outputStream.write(bb2);
                    break;
                case 3:
                    outputStream.write(bb3);
                    break;
            }

            switch (align){
                case 0:
                    //left align
                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            outputStream.write(msg.getBytes());
            outputStream.write(PrinterCommands.LF);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
