package com.example.dell.pos;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class UserAreaActivity extends AppCompatActivity {

    Button btn_logout;
    TextView txt_id, txt_username;
    String user_id, username;
    SharedPreferences sharedpreferences;

    public static final String TAG_ID = "user_id";
    public static final String TAG_USERNAME = "username";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_area);

        txt_id = (TextView) findViewById(R.id.txt_id);
        txt_username = (TextView) findViewById(R.id.txt_username);
        btn_logout = (Button) findViewById(R.id.btn_logout);

        sharedpreferences = getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);

        user_id = getIntent().getStringExtra(TAG_ID);
        username = getIntent().getStringExtra(TAG_USERNAME);

        txt_id.setText("ID : " + user_id);
        txt_username.setText("USERNAME : " + username);

        btn_logout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                // update login session ke FALSE dan mengosongkan nilai id dan username
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean(LoginActivity.session_status, false);
                editor.putString(TAG_ID, null);
                editor.putString(TAG_USERNAME, null);
                editor.commit();

                Intent intent = new Intent(UserAreaActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
        });


//        final EditText etUsername = (EditText) findViewById(R.id.etUsername);
//        final EditText etAge= (EditText) findViewById(R.id.etAge);
//        final TextView welcomeMessage = (TextView) findViewById(R.id.tvWelcomeMsg);
//
//        Intent intent = getIntent();
//        String name = intent.getStringExtra("name");
//        String username = intent.getStringExtra("username");
//        int age= intent.getIntExtra("age",-1);
//
//        String message = name + "Welcome to user are";
//        welcomeMessage.setText(message);
//        etUsername.setText(username);
//        etAge.setText(age + "");


    }
}
