package com.example.dell.pos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PrintActivity extends AppCompatActivity {

   Button btnSend;
   EditText etText;

    OutputStream outputStream;
    InputStream inputStream;
    Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);

        etText = (EditText) findViewById(R.id.etText);
        btnSend = (Button) findViewById(R.id.btnSend);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    printData();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

    }

    public void printData() throws IOException {
        try{
            String msg = etText.getText().toString();
            msg+="\n";
            outputStream.write(msg.getBytes());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
